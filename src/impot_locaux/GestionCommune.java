package impot_locaux;

import java.util.*;

public class GestionCommune {

	public static void main(String[] args) {
		int impotTotalInd = 0;
		int impotTotalPro = 0;
		int impotTotal;

		ArrayList<Habitation> habitationInd = new ArrayList<>();
		habitationInd.add(new HabitationIndividuelle("", "ici", 120, 5, false));
		habitationInd.add(new HabitationIndividuelle("lui", "la bas", 120, 8, true));

		ArrayList<Habitation> habitationPro = new ArrayList<>();
		habitationPro.add(new HabitationPro("toi", "ici", 110, 20));
		habitationPro.add(new HabitationPro("elle", "Marseille", 15, 19));
		habitationPro.add(new HabitationPro("eux", "Lille", 500, 2));

		for (Habitation ind : habitationInd) {
			System.out.println(ind);			
			impotTotalInd += ind.impot();
			System.out.println("total => " + impotTotalInd);
		}

		for (Habitation hPro : habitationPro) {
			System.out.println(hPro);
			impotTotalPro += hPro.impot();
		}	

		
		System.out.println("total => " + impotTotalPro);

		impotTotal = impotTotalInd + impotTotalPro;
		System.out.println("l'impot total de la communez est de " + impotTotalInd + " + " + impotTotalPro + " = " + impotTotal);
	}

}
