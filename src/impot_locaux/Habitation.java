package impot_locaux;

public class Habitation {
	protected String proprietaire;
	protected String adresse;
	protected int surface;

	public String getProprietaire() {
		return proprietaire;
	}

	public void setProprietaire(String proprietaire) {
		if (proprietaire == null || proprietaire.trim().equals("")) {
			throw new RuntimeException("Le proprietaire ne paut pas être vide");
		}
		this.proprietaire = proprietaire;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public int getSurface() {
		return surface;
	}

	public void setSurface(int surface) {
		this.surface = surface;
	}

	public Habitation(String p, String a, int s) {
		this.proprietaire = p;
		this.adresse = a;
		this.surface = s;
	}

	public int impot() {
		int montantImpot = 2 * surface;
		return montantImpot;
	}

	public void affiche() {
		System.out.println(this.toString());
	}

	@Override
	public String toString() {
		return "le proprietaire de l'habitation est  " + proprietaire + ", l'adresse est " + adresse
				+ ", la surface de la maison est de  " + surface + " m2";
	}
}
