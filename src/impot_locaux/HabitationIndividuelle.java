package impot_locaux;

public class HabitationIndividuelle extends Habitation {

	protected int nbrePieces;
	protected boolean piscine = false;


	public int getNbrePieces() {
		return nbrePieces;
	}

	public void setNbrePieces(int nbrePieces) {
		this.nbrePieces = nbrePieces;
	}

	public boolean isPiscine() {
		return piscine;
	}

	public void setPiscine(boolean piscine) {
		this.piscine = piscine;
	}

	public HabitationIndividuelle(String p, String a, int s, int nbrePieces, boolean piscine) {
		super(p, a, s);
		this.nbrePieces = nbrePieces;
		this.piscine = piscine;
	}

	public int impot() {
		int montantImpot;
		if (piscine) {
			montantImpot = (super.impot() + (100 * nbrePieces) + 500);
		} else {
			montantImpot = (super.impot()) + (100 * nbrePieces);
		}

		return montantImpot;
	}

	public static void main(String[] args) {
		HabitationIndividuelle ici = new HabitationIndividuelle("moi", "ici", 100, 10, false);
		System.out.println(ici.toString());
	}

	@Override
	public String toString() {
		return super.toString() + "\nc'est une Habitation Individuelle avec " + nbrePieces
				+ (" piéces et une piscine ?? => " + piscine + " l'impot sera donc de " + impot() + " €");
	}

}
