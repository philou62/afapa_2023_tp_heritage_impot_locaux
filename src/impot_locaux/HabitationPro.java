package impot_locaux;

public class HabitationPro extends Habitation {
	protected int nbreEmploye;

	public int getNbreEmploye() {
		return nbreEmploye;
	}

	public void setNbreEmploye(int nbreEmploye) {
		this.nbreEmploye = nbreEmploye;
	}

	public HabitationPro(String p, String a, int s, int nbreEmploye) {
		super(p, a, s);
		this.nbreEmploye = nbreEmploye;
	}

	public int impot() {

		return super.impot() + ((nbreEmploye / 10) * 1000);
	}

	@Override
	public String toString() {
		return super.toString() + (" c'est une entreprise qui a " + nbreEmploye + " employés" + " et paye donc "
				+ impot() + " euros d'impot");
	}

}
